var format = require('cloud/util.js').format;

exports.base = 'https://graph.facebook.com';

exports.update = function (user) {

  var domain = this.base;
  return Parse.Cloud.httpRequest({

    url: format(
      '%s/me?access_token=%s', 
      domain, user.get('fbToken'))

  }).then(function(httpResponse) {

    var data = httpResponse.data,
        name = data.name,
        idx  = name.lastIndexOf(' ');

    user.set('firstName'  , data.first_name);
    user.set('surname'    , data.last_name);
    user.set('displayName', name);
    user.set('birthday'   , new Date(data.birthday));
    user.set('gender'     , data.gender);
    user.set('email'      , data.email);
    user.set('username'   , data.username);
    user.set('fbUpdatedAt', new Date());

    return Parse.Cloud.httpRequest({
      url: format(
        '%s/%d/picture?%s&access_token=%s', 
        domain, data.id, 
        'width=96&height=96&redirect=0&type=normal',
        user.get('fbToken'))
    }).then(function(httpResponse) {
      user.set('picture', httpResponse.data.data.url);
      return user.save();
    });

  });
};