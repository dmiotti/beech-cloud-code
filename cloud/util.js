var formatRegExp = /%[sdj%]/g;

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}

exports.format = function(f) {
  if (typeof(f) != "string") {
    var objects = [];
    for (var i = 0; i < arguments.length; i++)
      objects.push(inspect(arguments[i]));
    return objects.join(' ');
  }

  var i = 1,
      args = arguments,
      len = args.length,
      str;

  str = String(f).replace(formatRegExp, function(x) {
    if (x === '%%') return '%';
    if (i >= len) return x;
    switch (x) {
      case '%s': return String(args[i++]);
      case '%d': return Number(args[i++]);
      case '%j':
        try {
          return JSON.stringify(args[i++]);
        } catch (_) {
          return '[Circular]';
        }
      default:
        return x;
    }
  });

  for (var x = args[i]; i < len; x = args[++i]) {
    if (x === null || !isObject(x)) {
      str += ' ' + x;
    } else {
      str += ' ' + inspect(x);
    }
  }

  return str;
};

exports.token = function() {
  var rand = function() {
    return Math.random().toString(36).substr(2) // remove `0.`
  }
  return rand() + rand()
}