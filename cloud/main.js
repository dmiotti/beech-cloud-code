var Facebook = require('cloud/remote/facebook.js'),
    format   = require('cloud/util.js').format;

Parse.Cloud.define("UpdateFbUser", function(req, res) {
  Parse.Cloud.useMasterKey();

  var user = new Parse.User();
  user.id = req.params.user_id;
  user.set('fbToken', req.params.fb_token);
  return Facebook.update(user).then(function() {
    return res.success(
      format('User %s updated from facebook', 
        user.id));
  }).fail(function (error) {
    return res.error(
      format(
        'Failed to update user %s from Facebook (%j)',
        req.params.user_id, error));
  });
});

Parse.Cloud.beforeSave(Parse.User, function(req, res) {
  var user = req.object;
  if (!user.existed()) {
    if (Parse.FacebookUtils.isLinked(user)) {
      user.set('fbToken', user.get('authData').facebook.access_token);
    }
  }
  return res.success();
});

Parse.Cloud.afterSave(Parse.User, function(req) {
  var user = req.object,
      params = { user_id: user.id };
  if (user.get('fbToken') && !user.get('fbUpdatedAt')) {
    params['fb_token'] = user.get('fbToken');
    Parse.Cloud.run('UpdateFbUser', params);
  }
});
